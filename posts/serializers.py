# posts/serializers.py
from rest_framework import serializers
from .models import Posts, FeaturedPost, Comment


class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        exclude = ['id', 'author', 'post']   # You can specify fields explicitly if needed


class PostsSerializer(serializers.ModelSerializer):
    comments = CommentsSerializer(many=True, read_only=True)
    category_name = serializers.SerializerMethodField()
    author_name = serializers.SerializerMethodField()

    class Meta:
        model = Posts
        fields = ['title', 'author_name', 'content', 'date_posted', 'category_name', 'comments']  # You can specify fields explicitly if needed

    def get_category_name(self, obj):
        return obj.category.name if obj.category else None

    def get_author_name(self, obj):
        return obj.author.username if obj.author else None


class FeaturedPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeaturedPost
        fields = '__all__'  # You can specify specific fields if needed